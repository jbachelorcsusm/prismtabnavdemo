# Simple Prism Tabbed Nav App With IActiveAware Exercise #

This is a very simple tabbed app demo meant to teach the implementation of and demonstrate the Prism IActiveAware interface in a Xamarin.Forms tabbed app.

### Exercise Steps
* Start from the ['ExerciseStart'](https://bitbucket.org/jbachelorcsusm/prismtabnavdemo/get/ExerciseStart.zip) tag of this repository.
* Add ViewModel classes to the ViewModels folder (and namespace) for all 3 tab pages. Make sure to give each a Title property of type string. Hint: these will be just like the TabContainerPageViewModel for now. Make sure to include the Title property, and set it in the ViewModel constructor.
* Register all pages with their ViewModels in App.xaml.cs.
* Set the initial navigation call in the OnInitialized method in App.xaml.cs.
* Run the app to make sure it is working properly... You should be able to see 3 tabbed pages, and tap the tabs to navigate to any one of them.
* In order to get notifications as to when each tab becomes active/inactive, implement the IActiveAware interface in all 3 tab pages. It would probably be a good idea to start with just one, then run the app to make sure it works. Use debug statements to prove that your IsActiveChanged event handler is wired up correctly. Once you have one done, it will be fairly straight-forward to implement it in the other two. Use this illustrated screenshot as an example to guide you: 
![Sample tab page ViewModel](TabPageViewModelSample.png "Sample tab page ViewModel")

### Extra Challenges
* Challenge Option:  Implement INavigationAware in all ViewModels and add debug statements to see when these events are (and are not) called. This will help illustrate why INavigationAware is not always enough, and why we need IActiveAware.
* Challenge Option:  Using inheritance, see if you can figure out how to make a ViewModel base class and move your implementation of IActiveAware there, so you only have to do all the wiring once. This will help you reduce the amount of code in each ViewModel (less code == less bugs), and helps you follow the [DRY (Don't Repeat Yourself) principle](http://deviq.com/don-t-repeat-yourself/) of software development.

### Why IActiveAware? ###

In regular (hierarchical) navigation, we can use [INavigationAware](http://prismlibrary.github.io/docs/xamarin-forms/Navigation-Service.html) from Prism in order to have our ViewModels know when they become active. When it comes to a tabbed app, we unfortunately do not get these events. However, by implementing the IActiveAware interface provided by Prism, we can get notifications of when each tab becomes active and inactive. There is no equivalent to the INavigationAware NavigationParameters.

### Application Output ###

Be sure to have your ['Application Output' pad](https://developer.xamarin.com/recipes/cross-platform/ide/debugging/output_information_to_log_window/) visible when you run the app... The debug statements will be demonstrating the ViewModel's response to becoming active and inactive.
